MORE MODELS WANTED
------------------

(devices) last modified: 23.01.2022

## opamp

Add models for jellybean opamps.
  * ~~uA741 = 1/2 MC1458, RC1558~~
  * ~~LM358 = LM2902~~
  * TL06x, ~~TL07x, TL08x~~
  * ~~NE5534~~, NE5532

  * TS272 CMOS - lowpower
  * B176D = uA776 (programmable opamp) -> use in Polivoks filterclone anyways ...

  * LM13700 transconductance

(Instead of a list of equivalence use symlinks.)

## diode

Add models for diodes I have in stock.
  * Schottky
  * 1N60 Germanium ?
  * Zener ?

## transistor/bjt

Add models for transistors I have in stock.
  * bipolar
    - 2N3904       
    - ~~BC547, BC557~~
    - BC327, BC337
    - BC517, BC516 (Darlington)
    - BD139, BD140
    - BD
  * jfet
    - BF256B
  * mosfet
    - BS170
    - IRLZ24 (?)
    - IRF3205

  * UJT BT33 - haha never find one for this obscure asia Tincan

## regulator

Regulators
  * TL431
  * ~~MC33063~~

## logic

Logic
  * 74xx
    - good analog model of 74AC86 (xor) -> mixer
  * 4000
    - analog model of CD4069UB ?
  * ~~LM339 comparator~~

## misc

Try to build a model for signal and flyback transformers 
I have a bag of... (Pollin)

---

Future taskell wishlist

## Wanted

## Needs Formatting

## Needs Improvement

## Done
