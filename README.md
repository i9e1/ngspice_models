small ngspice library to use in kicad
-------------------------------------

This library should contain generic models and 
simple models for some jellybean parts to simulate 
along with design in kicad.

The focus or usecase as of 01/2022 is Synth DIY / Audio 
and simple uC projects (like Arduino).

Im on the hunt for good small signal 
in somewhat audio range (<30kHz).


Naming convention: 
  + caps
  + without special part numbers only when needed, 
  + underscore
  + model provider / semi manufacturer
  + *.mod*
e.g. LM386_national.mod

A big Todo is to merge different model files for one device.
Also using the datasheet pinnumbers / kicad pinnumbers would be great!

(autogenerate index)
